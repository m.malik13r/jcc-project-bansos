import React from "react";
import logo from "./asset/img/logo.png";
import { useState } from "react";

const FormBansos = () => {
  let [dataWarga, setDataWarga] = useState([]);
  let [gender, setGender] = useState("Laki-Laki");
  let [alasan, setAlasan] = useState("Kehilangan pekerjaan");
  let [lainnya, setLainnya] = useState();
  let [checked, setChecked] = useState(false);

  const [input, setInput] = useState({
    nama: " ",
    nik: 0,
    nkk: 0,
    umur: 0,
    gender: `${gender}`,
    alamat: " ",
    rt: " ",
    rw: " ",
    prv_penghasilan: 0,
    aft_penghasilan: 0,
    alasan: `${alasan}`,
    confirm: `${checked}`,
  });

  const handleChange = (event) => {
    let { name, value } = event.target;
    setInput({ ...input, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let {
      nama,
      nik,
      nkk,
      umur,
      gender,
      alamat,
      rt,
      rw,
      prv_penghasilan,
      aft_penghasilan,
      alasan,
      confirm,
    } = input;
    console.log(input);
    let data = dataWarga;
    data = [
      ...data,
      {
        nama,
        nik,
        nkk,
        umur,
        gender,
        alamat,
        rt,
        rw,
        prv_penghasilan,
        aft_penghasilan,
        alasan,
        confirm,
      },
    ];
    setDataWarga([...data]);
    setInput({
      nama: " ",
      nik: 0,
      nkk: 0,
      umur: 0,
      gender: `${gender}`,
      alamat: " ",
      rt: " ",
      rw: " ",
      prv_penghasilan: 0,
      aft_penghasilan: 0,
      alasan: `${alasan}`,
      confirm: `${checked}`,
    });
  };

  const handleGender = (e) => {
    setGender(e.target.value);
    let { name, value } = e.target;
    setInput({ ...input, [name]: value });
  };
  const handleAlasan = (e) => {
    if (alasan !== 1) {
      setLainnya(null);
      setAlasan(e.target.value);
      let { name, value } = e.target;
      setInput({ ...input, [name]: value });
    } else {
      setAlasan(lainnya);
      console.log(alasan);
      let { name, value } = e.target;
      setInput({ ...input, [name]: value });
    }
  };
  const handleLainnya = (e) => {
    setLainnya(e.target.value);
    let { name, value } = e.target;
    setInput({ ...input, [name]: value });
  };
  const handleCheck = (e) => {
    setChecked(!checked);
    let { name, value } = e.target;
    setInput({ ...input, [name]: value });
  };

  return (
    <div className="mb-40">
      <div className="bg-green-700 py-2">
        <img className="rounded-full mx-auto bg-green-500" src={logo} alt="Logo"/>
      </div>
      <div className="bg-green-600 w-full py-2 text-white font-medium">
        <p className="text-2xl text-center">Form Pendataan Bansos</p>
      </div>
      <div className="sm:mx-auto px-4 sm:w-11/12">
        <form onSubmit={handleSubmit}>
          <div className="my-4">
            <label className="text-lg">
              Nama
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <input
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="text"
              name="nama"
              placeholder="Nama Lengkap Sesuai KTP"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              NIK
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <input
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="number"
              name="nik"
              placeholder="NIK pada KTP"
              pattern="[0-9]{16,}"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              Nomor Kartu Keluarga
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <input
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="number"
              name="nkk"
              placeholder="Nomor Kartu Keluarga"
              pattern="[0-9]{16,}"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              Foto KTP
              <span className="text-red-500 -dot">
                *<br />
              </span>
              <span className="text-sm">
                Maksimal 2MB, format JPG/JPEG/PNG/BMP<br/>
              </span>
            </label>
            <input
              //   className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="file"
              name="f_ktp"
              accept="image/png, image/jpg, image/bmp, image/jpeg"
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              Foto Kartu Keluarga
              <span className="text-red-500 -dot">
                *<br />
              </span>
              <span className="text-sm">
                Maksimal 2MB, format JPG/JPEG/PNG/BMP<br/>
              </span>
            </label>
            <input
              //   className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="file"
              name="f_kk"
              accept="image/png, image/jpg, image/bmp, image/jpeg"
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              Umur
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <input
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="number"
              name="umur"
              placeholder="Minimal 25 Tahun"
              min="25"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              Jenis Kelamin
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <select
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              name="gender"
              value={gender}
              onChange={handleGender}
            >
              <option value="Laki-Laki">Laki-Laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
          </div>
          <div className="my-4">
            <label className="text-lg">
              Alamat
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <textarea
              rows="4"
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full"
              type="text"
              name="alamat"
              placeholder="Alamat Lengkap Sesuai KTP"
              max="255"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              RT
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <input
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="text"
              name="rt"
              placeholder="RT Sesuai KTP"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              RW
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <input
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="text"
              name="rw"
              placeholder="RW Sesuai KTP"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              Penghasilan sebelum pandemi
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <input
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="number"
              name="prv_penghasilan"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              Penghasilan setelah pandemi
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <input
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              type="number"
              name="aft_penghasilan"
              onChange={handleChange}
              required
            />
          </div>
          <div className="my-4">
            <label className="text-lg">
              Alasan membutuhkan bantuan
              <span className="text-red-500 -dot">
                *<br />
              </span>
            </label>
            <select
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full h-10"
              name="alasan"
              onChange={handleAlasan}
              value={alasan}
              required
            >
              <option value="Kehilangan pekerjaan">Kehilangan pekerjaan</option>
              <option value="Kepala keluarga terdampak atau korban Covid">
                Kepala keluarga terdampak atau korban Covid
              </option>
              <option value="Tergolong fakir/miskin semenjak sebelum Covid">
                Tergolong fakir/miskin semenjak sebelum Covid
              </option>
              <option value="1">Lainnya...</option>
            </select>
            <textarea
              rows="4"
              className="border-green-700 px-4 py-1 text-lg focus:border-yellow-500 rounded-md border w-full my-2"
              type="text"
              name="alasan"
              placeholder="Alasan Lainnya"
              onChange={handleLainnya}
            />
          </div>
          <div className="my-4 flex -ml-8 items-center">
            <input
              className=" checked:bg-black px-4 py-1 text-sm focus:border-yellow-500 rounded-md border w-full h-10 "
              type="checkbox"
              name="confirm"
              value="agree"
              checked={checked}
              onChange={handleCheck}
            />
            <label>
              Saya menyatakan bahwa data yang diisikan adalah benar dan siap
              mempertanggungjawabkan apabila ditemukan ketidaksesuaian dalam
              data tersebut.
            </label>
          </div>
          <div>
            <input
              type="submit"
              className="text-white bg-green-700 rounded-md px-4 py-2 hover:bg-green-800 focus:bg-green-800 focus:border-yellow-500"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default FormBansos;
