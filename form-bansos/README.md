Nama    : Muhammad malik
Kelas   : ReactJS
Netlify : https://elegant-kelpie-bf3693.netlify.app/

Untuk usia 40-50 dimana performa mata manusia mulai menurun, hal-hal yang terlihat kecil mulai sulit untuk dilihat.
Oleh karena itu, memperbesar seluruh komponen merupakan langkah awal, seperti memperbesar font size dan menambah jarak antar komponen.
Penggunaan warna yang cerah memberi kesan positif sesuai dengan tujuan aplikasi.
Menggunakan perubahan warna pada state tertentu (active dll) membantu pengguna untuk lebih mudah memasukan data dengan benar karena pengguna tahu komponen mana yang sedang digunakan.